package com.m.gtgameon.model

data class GameOnRandomNumber(
    val number: Int
)
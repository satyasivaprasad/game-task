package com.m.gtgameon

import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.m.gtgameon.utils.AppConstants
import io.socket.client.IO
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    companion object {
        val LOG_TAG: String = MainActivity::class.java.simpleName
    }

    lateinit var socket: Socket
    private var evenNumberSum: Int = 0
    private var oddNumberSum: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        socket = IO.socket(AppConstants.SERVER_URL)
        socket.on(Socket.EVENT_CONNECT) {
            val emptyPayload = JSONObject()
            socket.emit("random", emptyPayload)
            runOnUiThread {
                Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show()
            }
        }
        socket.on(Socket.EVENT_DISCONNECT) {
            runOnUiThread {
                Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show()
            }
        }

        socket.on(Socket.EVENT_CONNECT_ERROR) {
            runOnUiThread {
                Toast.makeText(this, "Failed to connect", Toast.LENGTH_SHORT).show()
            }
        }

        socket.connect()

        socket.on("getNumber") {
            val result = it.get(0) as JSONObject
            val resultNumber = result.getInt("number")
            if (resultNumber % 2 == 0) {
                evenNumberSum++
                runOnUiThread {
                    latest_even_number.text = resultNumber.toString()
                    total_even_number.text = evenNumberSum.toString()
                }
            } else {
                oddNumberSum++
                runOnUiThread {
                    latest_odd_number.text = resultNumber.toString()
                    total_odd_number.text = evenNumberSum.toString()
                }
            }
            Log.v(LOG_TAG, "Check result $it")
            val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
            toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)
        }

        connectBtn.setOnClickListener {
            if (!socket.connected()) {
                socket.connect()
            } else {
                Toast.makeText(this, "Already Connected", Toast.LENGTH_SHORT).show()
            }
        }

        disConnectBtn.setOnClickListener {
            if (socket.connected()) {
                socket.disconnect()
            } else {
                Toast.makeText(this, "Already Disconnected", Toast.LENGTH_SHORT).show()
            }
        }
    }
}